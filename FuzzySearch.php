<?php

class FuzzySearch{
	/**
	 * N-gram の N の値
	 * @var int
	 */
	const N_SIZE = 2;

	/**
	 * MongoDB データベース
	 * @var MongoDB
	 */
	private $db;

	/**
	 * word コレクション
	 * @var MongoCollection
	 */
	private $wordCollection;

	/**
	 * wordNgram コレクション
	 * @var MongoCollection
	 */
	private $wordNgramCollection;

	/**
	 * Unicode NFKC 正規化処理クラス
	 * @var Normalizer
	 */
	private $normalizer;

	/**
	 * コンストラクタ
	 * @param MongoDB 操作対象となるデータベースのインスタンス
	 */
	public function __construct(MongoDB $mongoDB){
		$this->normalizer = new Normalizer();
		$this->db = $mongoDB;
		$this->wordCollection = $this->db->word;
		$this->wordCollection->ensureIndex(array("w" => 1), array("unique" => true));
		$this->wordCollection->ensureIndex(array("a" => 1));
		$this->wordNgramCollection = $this->db->wordNgram;
		$this->wordNgramCollection->ensureIndex(array("w" => 1), array("unique" => true));
	}

	/**
	 * 文字列から N-gram を作成
	 * Unicode NFKC 正規化を行った上で、英字を小文字化、カタカナをひらがなに変換してから N-gram を作成する
	 * @param string $s
	 * @return array
	 */
	private function makeNgram($s){
		$s = $this->normalizer->normalize($s, Normalizer::FORM_KC);
		$s = strtolower($s);
		$s = mb_convert_kana($s, "c", "UTF-8");
		$ret = array();
		$len = mb_strlen($s, "UTF-8");
		for($i = 0; $i < $len; ++$i){
			$v = mb_substr($s, $i, self::N_SIZE, "UTF-8");
			$ret[] = $v;
		}
		return $ret;
	}

	/**
	 * $s を N-gram で分割したデータを $id を参照する形で DB に登録する
	 * @param string/MongoId $id
	 * @param string $s
	 */
	private function addNgram($id, $s){
		$ngramSet = $this->makeNgram($s);
		$ngramSetCount = count($ngramSet);
		foreach($ngramSet as $idx => $ngram){
			$this->wordNgramCollection->update(
				array("w" => $ngram),
				array(
					'$push' => array(
						"p" => array(
							"r" => $id,
							"p" => $idx,
							"l" => $ngramSetCount,
						),
					),
				),
				array("upsert" => true, "safe" => true)
			);
		}
	}

	/**
	 * addNgram で登録されたデータを削除する
	 * @param string/MongoId $id
	 * @param string $s
	 */
	private function removeNgram($id, $s){
		$ngramSet = $this->makeNgram($s);
		$ngramSetCount = count($ngramSet);
		foreach($ngramSet as $idx => $ngram){
			$this->wordNgramCollection->update(
				array("w" => $ngram),
				array(
					'$pull' => array(
						"p" => array(
							"r" => $id,
							"p" => $idx,
							"l" => $ngramSetCount,
						),
					),
				),
				array("safe" => true)
			);
		}
	}

	/**
	 * 語句をDBに登録
	 * 重複項目を登録しようとした場合は失敗する
	 * @param string $word
	 * @return array/bool 登録したドキュメントの情報、失敗した場合はfalse
	 */
	public function addWord($word){
		//語句を登録
		$wordDoc = array(
			"w" => $word,
		);

		try{
			$this->wordCollection->insert($wordDoc, array("safe"=>true));
		} catch(MongoCursorException $e){
			if ($e->getCode() == 11000){
				//重複で失敗
				return false;
			} else {
				throw $e;
			}
		}

		//N-gramを登録
		$this->addNgram($wordDoc["_id"], $word);
	
		return $wordDoc;
	}

	/**
	 * 既に登録されている語句に対してエイリアスを登録する
	 * 語句が存在しなかったり、重複項目を登録しようとした場合は失敗する
	 * @param string $word 語句
	 * @param string $alias エイリアス名
	 * @return bool 成功したらtrue
	 */
	public function addWordAlias($word, $alias){
		//登録されている語句を探す
		$wordDoc = $this->wordCollection->findOne(array("w" => $word));
		if (!$wordDoc) return false;

		//エイリアスを登録
		$r = $this->wordCollection->update(
			array(
				"_id" => $wordDoc["_id"],
				"a" => array(
					'$ne' => $alias,
				),
			),
			array(
				'$push' => array(
					"a" => $alias,
				),
			),
			array("safe" => true)
		);
		if (!$r["updatedExisting"]){
			//項目が見つからない、あるいは重複
			return false;
		}

		//N-gramを登録
		$this->addNgram($wordDoc["_id"], $alias);

		return true;
	}

	/**
	 * 語句からエイリアス名を削除する
	 * @param string $word 語句
	 * @param string $alias エイリアス名
	 * @return bool 正常に削除できたらtrue
	 */
	public function removeWordAlias($word, $alias){
		//登録されている語句を探す
		$wordDoc = $this->wordCollection->findOne(array("w" => $word));
		if (!$wordDoc) return false;

		//N-gramを削除
		$this->removeNgram($wordDoc["_id"], $alias);

		//エイリアスを削除
		$r = $this->wordCollection->update(
			array("_id" => $wordDoc["_id"]),
			array(
				'$pull' => array(
					"a" => $alias,
				),
			),
			array("safe" => true)
		);
		return $r["updatedExisting"];
	}

	/**
	 * 語句をDBから削除
	 * @param string $word 語句
	 * @return bool 正常に削除できたらtrue
	 */
	public function removeWord($word){
		//登録されている語句を探す
		$wordDoc = $this->wordCollection->findOne(array("w" => $word));
		if (!$wordDoc) return false;

		//全てのエイリアスを消す
		if (isset($wordDoc["a"]) && is_array($wordDoc["a"])){
			foreach($wordDoc["a"] as $alias){
				$this->removeWordAlias($wordDoc["w"], $alias);
			}
		}

		//N-gramを削除
		$this->removeNgram($wordDoc["_id"], $wordDoc["w"]);
	
		//語句を削除
		$this->wordCollection->remove(array("_id" => $wordDoc["_id"]), array("justOne" => true, "safe" => true));

		return true;
	}

	/**
	 * 語句をあいまい検索する
	 * 返ってくる MongoCollection はランダムな名前で生成されるので使用後 drop すること
	 * @param string $word
	 * @return MongoCollection
	 */
	public function findSimilarWord($word){
		$map = new MongoCode(<<<EOCODE
function(){
	this.p.forEach(function(pi){
		emit(pi.r, {p:[pi.p], l:pi.l});
	});
}
EOCODE
		);

		$reduce = new MongoCode(<<<EOCODE
function(key, values) {
	var p = [], l, push = Array.prototype.push;
	values.forEach(function(value){
		push.apply(p, value.p);
		l = value.l;
	});
	return {p:p, l:l};
}
EOCODE
		);

		$finalize = new MongoCode(<<<EOCODE
function(key, value) {
	var score = 0, c = 0, prevPos = -2;
	value.p.sort(sortFunc);
	value.p.forEach(function(pi){
		if (pi == ++prevPos){
			++c;
		} else {
			score += Math.pow(3, c);
			score -= Math.abs(pi - prevPos - 1)*0.01;
			c = 0;
			prevPos = pi;
		}
	});
	score += Math.pow(3,c);
	score -= Math.abs(value.l - prevPos - 1)*0.01;
	return score;
}
EOCODE
		);

		$ngramSet = $this->makeNgram($word);
		if (count($ngramSet) > 1){
			array_pop($ngramSet);
		}
	
		$mr = $this->db->command(array(
			"mapReduce" => "wordNgram", 
			"map" => $map,
			"reduce" => $reduce,
			"finalize" => $finalize,
			"query" => array("w" => array('$in' => $ngramSet)),
			"scope" => array(
				"sortFunc" => new MongoCode("function(a,b){ return a == b ? 0 : a < b ? -1 : 1; }"),
			),
			"out" => uniqid("nearTitle", true),
		));
		return $this->db->selectCollection($mr["result"]);
	}

	/**
	 * ドキュメントのIDから対応する語句を求める
	 * @param string/MongoId $id
	 */
	public function idToWord($id){
		return $this->wordCollection->findOne(array("_id" => $id));
	}

	/**
	 * データの総数を返す
	 * @return int
	 */
	public function count(){
		return $this->wordCollection->count();
	}
}

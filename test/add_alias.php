<?php
if (PHP_SAPI != "cli") exit;

if (count($argv) <= 2){
	echo <<<EOUSAGE
usage: php add_alias.php registered-word alias [alias...]

EOUSAGE;
	exit;
}

require_once '../FuzzySearch.php';

$mongo = new Mongo();
$ar = new FuzzySearch($mongo->animeTitle);
array_shift($argv);
$word = array_shift($argv);
foreach($argv as $i => $value){
	printf(
		"%s <- %s: %s\n",
		$word,
		$value,
		$ar->addWordAlias($word, $value) ? " alias added." : "failed to add alias."
	);
}

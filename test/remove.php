<?php
if (PHP_SAPI != "cli") exit;

if (count($argv) <= 1){
	echo <<<EOUSAGE
usage: php remove.php remove-word [remove-word...]

EOUSAGE;
	exit;
}

require_once '../FuzzySearch.php';

$mongo = new Mongo();
$ar = new FuzzySearch($mongo->animeTitle);
array_shift($argv);
foreach($argv as $i => $value){
	printf(
		"%s: %s\n",
		$value,
		$ar->removeWord($value) ? " removed." : "failed to remove."
	);
}

<?php
require_once "../FuzzySearch.php";

$mongo = new Mongo();
$ar = new FuzzySearch($mongo->animeTitle);

switch(@$_POST["mode"]){
case "search":
	$count = 0;
	$startTime = microtime(true);
	$collection = $ar->findSimilarWord($_POST["word"]);
	$endTime = microtime(true);
	printf("time: %f", $endTime - $startTime);
	echo "<ol>";
	try{
		foreach($collection->find()->sort(array("value"=>-1)) as $doc){
			$wordDoc = $ar->idToWord($doc["_id"]);
			echo "<li>";
			printf("%s <span class=\"score\">%0.2f</span><br />", htmlspecialchars($wordDoc["w"]), $doc["value"]);
			if (isset($wordDoc["a"]) && is_array($wordDoc["a"]) && count($wordDoc["a"])){
				printf("<span class=\"alias\">別名: %s</span>", implode(", ", $wordDoc["a"]));
			}
			echo "</li>";
			if (++$count == 10) break;
		}
	}catch(Exception $e){
		$collection->drop();
		throw $e;
	}
	$collection->drop();
	echo "</ol>";
	exit;
}
?>
<!DOCTYPE html><html lang="ja"><meta charset="UTF-8">
<style>
body{
font: 13px/1.231 "メイリオ","Meiryo","ＭＳ Ｐゴシック","Hiragino Kaku Gothic Pro","ヒラギノ角ゴ Pro W3",arial,helvetica,clean,sans-serif; 
}
h1 {
	font-size: 100%;
}
span.score {
	font-family: Helvetica, Arial, sans-serif;
	color: #ccc;
	font-size: 75%;
}
span.alias {
	color: #999;
	font-size: 75%;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
jQuery(function($){
$('#search, #add').submit(function(){
	var $this = $(this);
	$(':input').attr('disabled', 'disabled');
	$.post($this.attr('action'), {
		mode: $this.data('mode'),
		word: $this.find(':input[name=word]').val(),
		_: new Date().getTime()
	}, function(json){
		$($this.data('result')).html(json);
		$(':input').removeAttr('disabled');
	}, 'html');
	return false;
});
});
</script>
<title>アニメタイトルあいまい検索</title>
<h1>アニメタイトルあいまい検索</h1>
<p>全<?php echo $ar->count(); ?>件</p>
<form id="search" action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" data-mode="search" data-result="#search-result">
	<input type="text" name="word"><input type="submit" value="検索">
</form>
<div id="search-result"></div>
</html>
